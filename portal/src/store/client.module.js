import {ClientsService } from "@/common/api.service";
import { FETCH_CLIENTS } from "./actions.type";
import {
  FETCH_START,
  FETCH_END,
  UPDATE_CLIENT_IN_LIST
} from "./mutations.type";

const state = {
  clients: [],
  isLoading: true,
  clientsCount: 0
};

const getters = {
  clientsCount(state) {
    return state.clientsCount;
  },
  clients(state) {
    return state.clients;
  },
  isLoading(state) {
    return state.isLoading;
  }
};

const actions = {
  [FETCH_CLIENTS]({ commit }, params) {
    commit(FETCH_START);
    return ClientsService.query(params.type, params.filters)
      .then(({ data }) => {
        commit(FETCH_END, data);
      })
      .catch(error => {
        throw new Error(error);
      });
  }
};

/* eslint no-param-reassign: ["error", { "props": false }] */
const mutations = {
  [FETCH_START](state) {
    state.isLoading = true;
  },
  [FETCH_END](state, clients /* { clients, clientsCount }*/) {
    state.clients = clients;
    state.clientsCount = clients.length //clientsCount;
    state.isLoading = false;
  },
  [UPDATE_CLIENT_IN_LIST](state, data) {
    state.clients = state.clients.map(client => {
      if (client.slug !== data.slug) {
        return client;
      }
      // We could just return data, but it seems dangerous to
      // mix the results of different api calls, so we
      // protect ourselves by copying the information.
      client.favorited = data.favorited;
      client.favoritesCount = data.favoritesCount;
      return client;
    });
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
