export const PURGE_AUTH = "logOut";
export const SET_AUTH = "setUser";
export const SET_ERROR = "setError";
export const RESET_STATE = "resetModuleState";

export const FETCH_END = "setClients";
export const FETCH_START = "setLoading";
export const SET_CLIENT = "setClient";
export const SET_COMMENTS = "setComments";
export const UPDATE_CLIENT_IN_LIST = "updateClientInList";