import Vue from "vue";
import Vuex from "vuex";


import auth from "./auth.module";
import client from "./client.module";
import complaint from "./complaint.module";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    client,
    complaint
  }
});
