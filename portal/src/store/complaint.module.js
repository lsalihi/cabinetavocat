//import {ComplaintsService } from "@/common/api.service";
import { FETCH_COMPLAINTS } from "./actions.type";
import {
  FETCH_START,
  FETCH_END,
  UPDATE_COMPLAINT_IN_LIST
} from "./mutations.type";

const state = {
  complaints: [],
  isLoading: true,
  complaintsCount: 0
};

const getters = {
  complaintsCount(state) {
    return state.complaintsCount;
  },
  complaints(state) {
    return state.complaints;
  },
  isLoading(state) {
    return state.isLoading;
  }
};

const actions = {
  [FETCH_COMPLAINTS]({ commit }, params) {
    console.log(params)
    commit(FETCH_START);
    commit(FETCH_END, [
      {"client": "JB4476889", "numeroPlainte": "03012020", "sujetPlainte": "رهن عقاري مشترك", "commentaire": "ملف شائك", "type" : 'استثنائي' },
      {"client": "JB4476889", "numeroPlainte": "03012020", "sujetPlainte": "رهن عقاري مشترك", "dateCreation": null, "type" : 'استثنائي' },
      {"client": "JB4476889", "numeroPlainte": "03012020", "sujetPlainte": "رهن عقاري مشترك", "dateCreation": null, "type" : 'استثنائي' }
    ]);
    /*return ComplaintsService.query(params.type, params.filters)
      .then(({ data }) => {
        commit(FETCH_END, data);
      })
      .catch(error => {
        throw new Error(error);
      });*/
  }
};

/* eslint no-param-reassign: ["error", { "props": false }] */
const mutations = {
  [FETCH_START](state) {
    state.isLoading = true;
  },
  [FETCH_END](state, complaints /* { complaints, complaintsCount }*/) {
    state.complaints = complaints;
    state.complaintsCount = complaints.length //complaintsCount;
    state.isLoading = false;
  },
  [UPDATE_COMPLAINT_IN_LIST](state, data) {
    state.complaints = state.complaints.map(complaint => {
      if (complaint.slug !== data.slug) {
        return complaint;
      }
      // We could just return data, but it seems dangerous to
      // mix the results of different api calls, so we
      // protect ourselves by copying the information.
      complaint.favorited = data.favorited;
      complaint.favoritesCount = data.favoritesCount;
      return complaint;
    });
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
