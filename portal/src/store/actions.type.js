export const CHECK_AUTH = "checkAuth";
export const LOGIN = "login";
export const LOGOUT = "logout";
export const REGISTER = "register";
export const UPDATE_USER = "updateUser";

export const CLIENT_PUBLISH = "publishClient";
export const CLIENT_DELETE = "deleteClient";
export const CLIENT_EDIT = "editClient";
export const CLIENT_EDIT_ADD_TAG = "addTagToClient";
export const CLIENT_EDIT_REMOVE_TAG = "removeTagFromClient";
export const CLIENT_RESET_STATE = "resetClientState";
export const FETCH_CLIENT = "fetchClient";
export const FETCH_CLIENTS = "fetchClients";


export const COMPLAINT_PUBLISH = "publishComplaint";
export const COMPLAINT_DELETE = "deleteComplaint";
export const COMPLAINT_EDIT = "editComplaint";
export const COMPLAINT_EDIT_ADD_TAG = "addTagToComplaint";
export const COMPLAINT_EDIT_REMOVE_TAG = "removeTagFromComplaint";
export const COMPLAINT_RESET_STATE = "resetComplaintState";
export const FETCH_COMPLAINT = "fetchComplaint";
export const FETCH_COMPLAINTS = "fetchComplaints";