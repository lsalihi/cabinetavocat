import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "./translation/i18n"

import VueUploadComponent from "vue-upload-component";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

import Paginate from "vuejs-paginate";

import "./registerServiceWorker";

import { CHECK_AUTH } from "./store/actions.type";
import ApiService from "./common/api.service";
import DateFilter from "./common/date.filter";
import DateHeureFilter from "./common/dateHeure.filter";
import ErrorFilter from "./common/error.filter";
import FormatSizeFilter from "./common/formatSize.filter";

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCheckCircle, faDoorOpen, faDownload, faEdit, faEye, faFileAlt, faFilePdf, faFileUpload, faFileWord, faFolder, faFolderMinus, faFolderOpen, faFolderPlus, faImage, faInfoCircle, faLayerGroup, faReply, faTimes, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'


library.add(faFilePdf, faFileWord, faImage, faFolderPlus, faFolder, faFolderMinus, 
  faFolderOpen, faFileAlt, faReply, faLayerGroup, faInfoCircle, faTrashAlt, faEdit, faTimes,
  faCheckCircle, faFolderPlus, faDownload, faDoorOpen, faEye, faFileUpload)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers)
Vue.component('font-awesome-layers-text', FontAwesomeLayersText)

Vue.config.productionTip = false;

Vue.component("file-upload", VueUploadComponent);
Vue.component("paginate", Paginate);

Vue.filter('formatSize', FormatSizeFilter);
Vue.filter("date", DateFilter);
Vue.filter("dateHeure", DateHeureFilter);
Vue.filter("error", ErrorFilter);

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

ApiService.init();

// Ensure we checked auth before each page load.
router.beforeEach((to, from, next) =>
  Promise.all([store.dispatch(CHECK_AUTH)]).then(next)
);
new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
