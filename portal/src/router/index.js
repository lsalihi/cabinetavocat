import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Codification from "../views/Codification.vue";
import Client from "../views/Client.vue";
import ClientEdit from "../views/ClientEdit.vue";
import Plainte from "../views/Plainte.vue";
import ComplaintEdit from "../views/PlainteEdit.vue";
import ComplaintManagement from "../views/PlainteManagement.vue";
import DocumentEdit from "@/views/DocumentEditV0.vue"

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/home",
    name: "Home",
    component: Home
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/clients",
    name: "Client",
    component: Client
  },
  {
    path: "/clientEdit",
    name: "ClientEdit",
    component: ClientEdit
  },  
  {
    path: "/complaints",
    name: "Complaint",
    component: Plainte
  },
  {
    path: "/complaintEdit",
    name: "ComplaintEdit",
    component: ComplaintEdit
  }, 
  {
    path: "/complaintManagement/:numero",
    name: "ComplaintManagement",
    component: ComplaintManagement
  }, 
  {
    path: "/codifications",
    name: "Codification",
    component: Codification
  },
  {
    path: "/import",
    name: "DocumentEdit",
    component: DocumentEdit
  },
  
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    name: "document-edit",
    path: "/editor/:slug?",
    props: true,
    component: () => import("@/views/DocumentEdit")
  },
  {
    // will match everything
    path: '*'
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
