export default{
    fr:{
        fr: "Français",
        ar: "العربية",
        Login: {
            seconnecter: "Se connecter",
            sedeconnecter: "Se déconnecter"
        },
        Header: {
            Accueil: "Accueil",
            Referentiel: "Référentiel",
            Codifications: "Codifications"
        }
    },
    ar:{
        ar: "العربية",
        fr: "Français",
        Login: {
            seconnecter: "تسجيل الدخول",
            sedeconnecter: "تسجيل الخروج"
        },
        Header: {
            Accueil: "الرئيسية",
            Referentiel: "الإعدادات",
            Codifications: "التقنين",
            Administration : 'إدراة النظام',
            CalendrierAndTasks : 'إدارة المهام والتقويمات',
            Clients: 'إدارة الزبناء',
            Dossiers : 'إدارة الملفات',
            Facturations : 'إدارة الفواتير ومراقبة الدفع',
            search: 'بحث',
            searchPlaceholder: 'رقم الملف أو رقم البطاقة الوطنية'
        },
        Codification: {
            searchPlaceholder: 'بحث',
            searchResult: 'نتيجة البحث',
            resultsFound: 'إعدادت على  ',
            noResultsFound: 'لم يسفر البحث عن أية نتائج ',
            messageNoResultsFound: "عذرا، لا توجد نتائج لبحثك!  المرجو استخذام اسم الموكل أو رقم البطاقة الوطنية",
            pages: 'صفحة/ات',
            next : 'التالي',
            prev: 'السابق'
        },
        ClientList:{
            fullName : 'الإسم الكامل',
            phoneNumner : 'الهاتف',
            adress: 'العنوان',
            email: 'البريد الإلكتروني',
            naissance: 'تاريخ الإزدياد',
            cin: 'البطاقة الوطنية',
            dateCreation : 'تاريخ التسجيل',
            dateModification: 'تاريخ التعديل',
            commentaire: 'ملاحظات',
            show_details: 'التفاصيل',
            nom: 'الإسم العائلي',
            prenom: 'الإسم الشخصي',
            noResultsFound: 'لم يسفر البحث عن أية نتائج ',
            loading: ' ... جاري البحث',
            pages: 'صفحة/ات',
            next : 'التالي',
            prev: 'السابق',
            add: 'موكِّل جديد'
        },
        ClientEdit:{
            title: 'معلومات الزبون (الموكِّل)',
            email: 'البريد الإلكتروني',
            phoneNumber : 'الهاتف',
            adress: 'العنوان',
            dateNaissance: 'تاريخ الإزدياد',
            cin: 'البطاقة الوطنية',
            commentaire: 'ملاحظات',
            nom: 'الإسم العائلي',
            prenom: 'الإسم الشخصي',
            fr: 'بالحروف اللاتينة',
            saisieAr: 'الكتابة العربية',
            clean : 'مسح',
            submit: 'تأكيد'
        },
        PlainteList:{
            noResultsFound: 'لم يسفر البحث عن أية نتائج ',
            loading: ' ... جاري البحث',
            pages: 'صفحة/ات',
            next : 'التالي',
            prev: 'السابق',
            add: 'ملف جديد',
            numeroPlainte: 'رقم الملف',
            sujetPlainte: 'الموضوع',
            dateCreation: 'تاريخ الإنشاء',
            dateModification: 'تاريخ التعديل',
            commentaire: 'ملاحظات',
            status: 'الوضعية',
            client: 'الموكِّل',
            type : 'طبيعة الملف'
        },
        ComplaintEdit:{
            title: 'معلومات الدعوة أو الملف',
            numero: 'رقم الملف',
            sujet : 'موضوع الملف',
            client: ' (رقم البطاقة الوطنية) الموكِّل',
            commentaire: 'ملاحظات',
            type : 'طبيعة الملف',
            saisieAr: 'الكتابة العربية',
            clean : 'مسح',
            submit: 'تأكيد'
        },
        ComplaintMangement:{
            title: 'إدارة الملف'
        },
        DossierInfo: {
          deleteMessage: "هل أنت متأكد من حذف هذا  المستند؟",
          searchPlaceholder : "بحث",
          products: "produits",
          command: "Commande N°",
          client: "Client",
          dateCreation: "Date création",
          newDossier: "مستند جديد",
          newDossierName: "إسم المستند",
          newDossierComment: "ملاحظات",
          uploadDossiers :"رفع وثائق",
          save : "حفظ",
          cancel : "رجوع"
        }
    }
}