import moment from "moment";

/*export default date => {
  return format(new Date(date), "MMMM D, YYYY");
};*/

export default date => {
  return moment(String(date)).format('DD/MM/YYYY')
};