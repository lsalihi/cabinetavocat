import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "@/common/jwt.service";
import { API_URL, API_URL_SERVICE } from "@/common/config";

const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = API_URL;
  },

  setHeader() {
    Vue.axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${JwtService.getToken()}`;
    Vue.axios.defaults.baseURL = API_URL_SERVICE;
  },

  query(resource, params) {
    return Vue.axios.get(resource, params).catch(error => {
      throw new Error(`[GWT] ApiService ${error}`);
    });
  },

  get(resource, slug = "") {
    return Vue.axios.get(`${resource}/${slug}`).catch(error => {
      throw new Error(`[GWT] ApiService ${error}`);
    });
  },

  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  delete(resource) {
    return Vue.axios.delete(resource).catch(error => {
      throw new Error(`[GWT] ApiService ${error}`);
    });
  }
};

export default ApiService;

export const ClientsService = {
  query(type, params) {
    return ApiService.query("clients" + (type === "feed" ? "/feed" : ""), {
      params: params
    });
  },
  get(slug) {
    return ApiService.get("clients", slug);
  },
  create(params) {
    return ApiService.post("clients", { client: params });
  },
  update(slug, params) {
    return ApiService.update("clients", slug, { client: params });
  },
  destroy(slug) {
    return ApiService.delete(`clients/${slug}`);
  }
};


export const ComplaintsService = {
  query(type, params) {
    return ApiService.query("complaints" + (type === "feed" ? "/feed" : ""), {
      params: params
    });
  },
  get(slug) {
    return ApiService.get("complaints", slug);
  },
  create(params) {
    return ApiService.post("complaints", { client: params });
  },
  update(slug, params) {
    return ApiService.update("complaints", slug, { client: params });
  },
  destroy(slug) {
    return ApiService.delete(`complaints/${slug}`);
  }
};
export const CodificationsService = {
};

export const CommentsService = {
};

export const FavoriteService = {
};
