import moment from "moment";

/*export default date => {
  return format(new Date(date), "MMMM D, YYYY");
};*/

export default dateHeure => {
  return moment(String(dateHeure)).format('DD/MM/YYYY hh:mm')
};