/**
 * View Models used by Spring MVC REST controllers.
 */
package ma.gestion.cav.web.rest.vm;
