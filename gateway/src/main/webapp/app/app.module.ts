import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { CavgatewaySharedModule } from 'app/shared/shared.module';
import { CavgatewayCoreModule } from 'app/core/core.module';
import { CavgatewayAppRoutingModule } from './app-routing.module';
import { CavgatewayHomeModule } from './home/home.module';
import { CavgatewayEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    CavgatewaySharedModule,
    CavgatewayCoreModule,
    CavgatewayHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    CavgatewayEntityModule,
    CavgatewayAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class CavgatewayAppModule {}
