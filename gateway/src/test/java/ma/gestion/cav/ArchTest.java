package ma.gestion.cav;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("ma.gestion.cav");

        noClasses()
            .that()
                .resideInAnyPackage("ma.gestion.cav.service..")
            .or()
                .resideInAnyPackage("ma.gestion.cav.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..ma.gestion.cav.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
